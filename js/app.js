let modal = document.querySelector("#modal"),
    modalOverlay = document.querySelector("#modal-overlay"),
    closeButton = document.querySelector("#modal-close-button"),
    openButton = document.querySelector("#modal-open-button"),
    list = document.querySelector("#list"),
    form = document.querySelector("form"),
    pagination = document.querySelector(".pagination"),
    total = document.querySelector(".total"),
    usersList = [],
    usersSort = "email",
    pagIterateNumber = 5;

init();

function init() {
    // Recupero los usuarios de localStorage
    getAllUsers();
    //Dibujo la paginación
    drawPagination();
    //Dibujo la primer pag
    drawUsersByPag(1);
}

// Formulario 

closeButton.addEventListener("click", function() {
    modal.classList.add("closed");
    modalOverlay.classList.add("closed");
    form.classList.remove("edit-flag");
    form.classList.add("new-flag");
    form.reset();
});

openButton.addEventListener("click", function() {
    modal.classList.remove("closed");
    modalOverlay.classList.remove("closed");
});

form.addEventListener("submit", submitForm);

function submitForm(e) {
    e.preventDefault();

    let user = {
        name: document.querySelector("#name").value,
        date: document.querySelector("#date").value,
        gender: document.querySelector(".gender:checked").value,
        email: document.querySelector("#email").value,
    }

    let originalEmail = document.querySelector("#original-email").value;
    if (originalEmail == "") {
        if (checkUser(user.email)) {
            save(user)
        } else {
            alert("El mail ya esta registrado");
        }
    } else {
        if (originalEmail != user.email) {
            removeUser(originalEmail)
        }
        save(user);
    }
}

function openEditForm(email) {

    modal.classList.toggle("closed");
    modalOverlay.classList.toggle("closed");

    form.classList.remove("new-flag");
    form.classList.add("edit-flag");

    let user = getUser(email);

    document.querySelector("#name").value = user.name;
    document.querySelector("#date").value = user.date;
    document.querySelector("#gender" + user.gender).checked = true;
    document.querySelector("#email").value = user.email;
    document.querySelector("#original-email").value = user.email;
}

function save(user) {
    setUser(user);

    modal.classList.toggle("closed");
    modalOverlay.classList.toggle("closed");

    form.classList.remove("edit-flag");
    form.classList.add("new-flag");

    form.reset();
    init();
}

function remove(email) {
    if (window.confirm("¿Estas seguro de eliminar al usuario?")) {
        removeUser(email);
        init();
    }
}

// localStorage 

function setUser(user) {
    localStorage.setItem(user.email, JSON.stringify(user));
}

function getUser(email) {
    return user = JSON.parse(localStorage.getItem(email));
}

function removeUser(email) {
    localStorage.removeItem(email);
}

function checkUser(email) {
    return localStorage.getItem(email) == null;
}

function getAllUsers() {
    usersList = [];
    Object.keys(localStorage).forEach(function(key) {
        usersList.push(JSON.parse(localStorage.getItem(key)));
    });
    usersList.sort(userListSort);
}

// Funcion de ordenamiento

function userListSort(a, b) {
    if (a[usersSort] > b[usersSort]) {
        return 1;
    }
    if (a[usersSort] < b[usersSort]) {
        return -1;
    }
    return 0;
};

// Paginación 

function drawPagination() {
    pagination.innerHTML = "";
    total.innerHTML = "";

    if (usersList.length) {
        let maxPag = Math.ceil(usersList.length / pagIterateNumber);
        for (let i = 1; i <= maxPag; i++) {
            let activeClass = ((i == 1) ? "active" : "");
            pagination.insertAdjacentHTML("beforeend", "<button id='p-" + i + "' class='button " + activeClass + "' onclick='drawUsersByPag(" + i + ")'>" + i + "</button>");
        }
        total.innerHTML = "Total de usuarios : " + usersList.length;
    }
}

function drawUsersByPag(pNumber) {
    list.innerHTML = "";
    let pActive = document.querySelector(".pagination > .active");
    if (pActive != null) {
        pActive.classList.remove("active");
        document.querySelector("#p-" + pNumber).classList.add("active");
    }

    if (usersList.length) {
        let max = pNumber * pagIterateNumber,
            min = max - pagIterateNumber;
        let userByPag = usersList.slice(min, max)
        userByPag.forEach(function(user) {
            list.insertAdjacentHTML("beforeend", drawUser(user));
        });
    }
}

// Plantilla para dibujar usuarios

function drawUser(user) {
    return "<div class='card'>" +
        "<div class='card-img'><img src='img/user-default.png' /></div>" +
        "<div class='card-info t-center'>" +
        `<button class="button remove" title="Eliminar" onclick=remove("${user.email}")></button>` +
        `<button class="button edit" title="Editar" onclick=openEditForm("${user.email}")></button>` +
        `<div><h2>${user.name}</h2></div>` +
        `<div>F. Nac:${user.date}</div>` +
        `<div>Gen:${user.gender}</div>` +
        `<div class='email'>${user.email}</div>` +
        "</div>" +
        "</div>";
}

function createUsers() {
    let date = new Date(),
        month = (date.getMonth() > 9) ? date.getMonth() : +"0" + date.getMonth().toString(),
        day = (date.getDate() > 9) ? date.getDate() : +"0" + date.getDate().toString();

    date = date.getFullYear() + "-" + month + "-" + day;

    for (let i = 0; i <= 9; i++) {
        setUser({
            name: "Nombre " + i,
            date: date,
            gender: "F",
            email: "email" + i + "@mail.com",
        });
    }

    init();
}